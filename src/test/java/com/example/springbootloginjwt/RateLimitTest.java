package com.example.springbootloginjwt;

import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author Abhishek Sheelwant
 */
@SpringBootTest(classes = {SpringBootLoginJwtApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class RateLimitTest {

    @Test
    public void rateLimiterTest() throws IOException {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            for (int i = 0; i < 2; i++) {
                HttpGet httpGet = new HttpGet("http://localhost:8080/api/test/all");
                try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
                    // Assert response code
                    String responseBody = readResponseBody(response.getEntity().getContent());
                    System.out.println("Response body: " + responseBody);

                    if (response.getStatusLine().getStatusCode() == 429) {
                        Assertions.assertEquals(429, response.getStatusLine().getStatusCode());
                        // Read response body
                        Header retryAfterHeader = response.getFirstHeader("Retry-After");
                        if (retryAfterHeader != null) {
                            System.out.println("Retry-After header value: " + retryAfterHeader.getValue());
                            Assertions.assertEquals("5", retryAfterHeader.getValue()); // Assert expected value
                        } else {
                            System.out.println("Retry-After header is not present");
                            // You can choose to fail the test if the header is expected but not present
                            Assertions.fail("Retry-After header is not present");
                        }
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String readResponseBody(InputStream inputStream) throws IOException {
        StringBuilder responseBody = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = reader.readLine()) != null) {
                responseBody.append(line);
            }
        }
        return responseBody.toString();
    }


}
