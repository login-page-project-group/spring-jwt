package com.example.springbootloginjwt.models;

public enum DepartmentEnum {
    HUMAN_RESOURCE,
    MARKETING,
    ENGINEERING
}
