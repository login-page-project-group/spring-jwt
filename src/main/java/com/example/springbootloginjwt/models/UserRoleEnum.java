package com.example.springbootloginjwt.models;

public enum UserRoleEnum {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
