package com.example.springbootloginjwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.example.springbootloginjwt.Repository")
public class SpringBootLoginJwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootLoginJwtApplication.class, args);
	}

}
