package com.example.springbootloginjwt.controllers;

import com.example.springbootloginjwt.Repository.RoleRepository;
import com.example.springbootloginjwt.Repository.UserRepository;
import com.example.springbootloginjwt.Security.jwt.JwtUtils;
import com.example.springbootloginjwt.Security.services.UserDetailsImpl;
import com.example.springbootloginjwt.models.Role;
import com.example.springbootloginjwt.models.User;
import com.example.springbootloginjwt.models.UserRoleEnum;
import com.example.springbootloginjwt.payload.Request.LoginRequest;
import com.example.springbootloginjwt.payload.Request.SignUpRequest;
import com.example.springbootloginjwt.payload.Response.MessageResponse;
import com.example.springbootloginjwt.payload.Response.UserInfoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@RestController
@CrossOrigin(origins = "http://localhost:8081", maxAge = 3600, allowCredentials = "true")
@RequestMapping("/api/auth")
public class AuthControllers {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtUtils jwtUtils;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        ResponseCookie jwtCookie = jwtUtils.generateJwtCookie(userDetails);

        List<String> roles = userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());

        return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, jwtCookie.toString()).body(new UserInfoResponse(userDetails.getId(), userDetails.getUsername(), userDetails.getEmail(), roles, userDetails.getFirstName(), userDetails.getLastName(), userDetails.getPhoneNo(), userDetails.getSalary(), userDetails.getDepartment()));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error : Username already in use !!"));
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error : Email already in use !!"));
        }

        User user = new User(signUpRequest.getUsername(), signUpRequest.getFirstName(), signUpRequest.getLastName(), signUpRequest.getPhoneNo(), signUpRequest.getSalary(), signUpRequest.getEmail(), passwordEncoder.encode(signUpRequest.getPassword()), signUpRequest.getDepartment(), signUpRequest.getRole());

        Set<Role> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findByName(UserRoleEnum.ROLE_USER).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role.getName()) {
                    case ROLE_ADMIN:
                        Role adminRole = roleRepository.findByName(UserRoleEnum.ROLE_ADMIN).orElseThrow(() -> new RuntimeException("Error : Role not found !!"));
                        roles.add(adminRole);
                        break;

                    case ROLE_MODERATOR:
                        Role modRole = roleRepository.findByName(UserRoleEnum.ROLE_MODERATOR).orElseThrow(() -> new RuntimeException("Error : Role not found !!"));
                        roles.add(modRole);
                        break;

                    default:
                        Role userRole = roleRepository.findByName(UserRoleEnum.ROLE_USER).orElseThrow(() -> new RuntimeException("Error : Role not found !!"));
                        roles.add(userRole);
                }
            });
        }
        user.setRoles(roles);
        userRepository.save(user);

        return ResponseEntity.ok(new MessageResponse("User registered Successfully!"));

    }

    @PostMapping("/signout")
    public ResponseEntity<?> logoutUser() {
        ResponseCookie cookie = jwtUtils.getCleanJwtCookie();
        return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, cookie.toString()).body(new MessageResponse("You've been signed out"));
    }

}
