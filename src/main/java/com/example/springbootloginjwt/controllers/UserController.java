package com.example.springbootloginjwt.controllers;


import com.example.springbootloginjwt.Security.services.UserService;
import com.example.springbootloginjwt.models.DepartmentEnum;
import com.example.springbootloginjwt.models.User;
import com.example.springbootloginjwt.models.UserRoleEnum;
import com.example.springbootloginjwt.payload.Response.MessageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:8081", maxAge = 5600, allowCredentials = "true")
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/all")
    public ResponseEntity<List<User>> getAllUser() {
        List<User> employees = userService.getAllUser();
        return new ResponseEntity<>(employees, HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<User> getUserById(@PathVariable(value = "id") Integer id) {
        User employee = userService.getASingleUser(id);
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<MessageResponse> addUser(@RequestBody User userRequest) {
        MessageResponse newUser = userService.createUser(userRequest);
        return new ResponseEntity<>(newUser, HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity<MessageResponse> updateUser(@RequestBody User user) {

        MessageResponse updateUser = userService.updateUser(user);
        return new ResponseEntity<>(updateUser, HttpStatus.OK);
    }

    @GetMapping("/delete/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") long id) {
        userService.deleteUser(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/department/all")
    public ResponseEntity<List<DepartmentEnum>> getAllDepartment() {
        List<DepartmentEnum> departmentEnumList = Arrays.asList(DepartmentEnum.values());
        return new ResponseEntity<>(departmentEnumList, HttpStatus.OK);
    }

    @GetMapping("/role/all")
    public ResponseEntity<List<UserRoleEnum>> getAllRoles() {
        List<UserRoleEnum> userRoleEnums = Arrays.asList(UserRoleEnum.values());
        return new ResponseEntity<>(userRoleEnums, HttpStatus.OK);
    }

    @PostMapping("/updatePassword")
    public ResponseEntity<MessageResponse> updatePassword(@RequestBody User userRequest) {
        System.out.println("incoming");
        MessageResponse updateUser = userService.updatePassword(userRequest);
        return new ResponseEntity<>(updateUser, HttpStatus.OK);
    }

    @GetMapping("/pagedAll")
    public ResponseEntity<Page<User>> getAllUsers(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<User> users = userService.getAllUsers(pageable);
        return ResponseEntity.ok(users);
    }

    @GetMapping("/filtered")
    public ResponseEntity<Page<User>> getUsersWithFilters(
            @RequestParam(required = false) String firstName,
            @RequestParam(required = false) String department,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "1") int size) {

        // Create specification based on filter criteria
        Specification<User> spec = Specification.where(null);
        if (firstName != null) {
            spec = spec.and((root, query, criteriaBuilder) ->
                    criteriaBuilder.like(root.get("firstName"), "%" + firstName + "%"));
        }
        if (department != null) {
            spec = spec.and((root, query, criteriaBuilder) ->
                    criteriaBuilder.equal(root.get("department"), department));
        }

        Pageable pageable = PageRequest.of(page, size);
        Page<User> users = userService.getUsersWithFilters(spec, pageable);
        return ResponseEntity.ok(users);
    }
}
