package com.example.springbootloginjwt.Security.services;


import com.example.springbootloginjwt.Exception.ResourceNotFoundException;
import com.example.springbootloginjwt.Repository.RoleRepository;
import com.example.springbootloginjwt.Repository.UserRepository;
import com.example.springbootloginjwt.models.Role;
import com.example.springbootloginjwt.models.User;
import com.example.springbootloginjwt.models.UserRoleEnum;
import com.example.springbootloginjwt.payload.Response.MessageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RoleRepository roleRepository;


    public UserServiceImpl() {

    }

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public MessageResponse createUser(User userRequest) {
        User newUser = new User();
        newUser.setFirstName(userRequest.getFirstName());
        newUser.setLastName(userRequest.getLastName());
        newUser.setPhoneNo(userRequest.getPhoneNo());
        newUser.setEmail(userRequest.getEmail());
        newUser.setSalary(userRequest.getSalary());
        newUser.setDepartment(userRequest.getDepartment());
        newUser.setUsername(userRequest.getUsername());
        newUser.setPassword(passwordEncoder.encode(userRequest.getPassword()));
        Set<Role> roles = new HashSet<>();
        for (UserRoleEnum userRoleEnum : userRequest.getUserRoles()) {
            Role userRole = roleRepository.findByName(userRoleEnum).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        }
        newUser.setRoles(roles);
        userRepository.save(newUser);
        return new MessageResponse("New User created successfully");
    }

    @Override
    public MessageResponse updateUser(User userRequest) throws ResourceNotFoundException {

        Optional<User> user = userRepository.findById(userRequest.getId());

        MessageResponse messageResponse;

        if (!user.isPresent()) {
            throw new ResourceNotFoundException("User", "id", userRequest.getId());
        } else {
            user.get().setFirstName(userRequest.getFirstName());
            user.get().setLastName(userRequest.getLastName());
            user.get().setPhoneNo(userRequest.getPhoneNo());
            user.get().setEmail(userRequest.getEmail());
            user.get().setSalary(userRequest.getSalary());
            user.get().setDepartment(userRequest.getDepartment());
            user.get().setUsername(userRequest.getUsername());
            user.get().setPassword(userRequest.getPassword());
            Set<Role> roles = new HashSet<>();
            for (UserRoleEnum userRoleEnum : userRequest.getUserRoles()) {
                Role userRole = roleRepository.findByName(userRoleEnum).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                roles.add(userRole);
            }
            user.get().setRoles(roles);

            userRepository.save(user.get());
            messageResponse = new MessageResponse("user updated");
            return messageResponse;
        }

    }

    @Override
    public User getASingleUser(long userId) throws ResourceNotFoundException {
        User user = userRepository.findById(userId).get();
        List<UserRoleEnum> userRoleEnumList = new ArrayList<>();
        for (Role role : user.getRoles()) {
            userRoleEnumList.add(role.getName());
        }
        user.setUserRoles(userRoleEnumList);
        return user;
    }

    @Override
    public List<User> getAllUser() {
        List<User> userList = userRepository.findAll();
        for (User user : userList) {
            List<UserRoleEnum> userRoleEnumList = new ArrayList<>();
            for (Role role : user.getRoles()) {
                userRoleEnumList.add(role.getName());
            }
            user.setUserRoles(userRoleEnumList);
        }
        return userList;
    }

    @Override
    public MessageResponse updatePassword(User userRequest) throws ResourceNotFoundException {
        Optional<User> user = userRepository.findById(userRequest.getId());
        user.get().setPassword(passwordEncoder.encode(userRequest.getPassword()));
        userRepository.save(user.get());
        MessageResponse messageResponse = new MessageResponse("Password Changed");
        return messageResponse;
    }

    @Override
    public void deleteUser(long userId) throws ResourceNotFoundException {
        User e = userRepository.findById(userId).get();
        if (e.getId() == userId) {
            userRepository.deleteById(userId);
        } else throw new ResourceNotFoundException("User", "id", userId);

    }

    @Override
    public Page<User> getAllUsers(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @Override
    public Page<User> getUsersWithFilters(Specification<User> spec, Pageable pageable) {
        return userRepository.findAll(spec, pageable);
    }
}
