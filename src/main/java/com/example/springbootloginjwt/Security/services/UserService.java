package com.example.springbootloginjwt.Security.services;

import com.example.springbootloginjwt.Exception.ResourceNotFoundException;
import com.example.springbootloginjwt.models.User;
import com.example.springbootloginjwt.payload.Response.MessageResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface UserService  {
    MessageResponse createUser(User userRequest);

    MessageResponse updateUser(User userRequest);

    MessageResponse updatePassword(User userRequest) throws ResourceNotFoundException;

    void deleteUser(long userId);

    User getASingleUser(long userId);

    List<User> getAllUser();

    Page<User> getAllUsers(Pageable pageable);

    Page<User> getUsersWithFilters(Specification<User> spec, Pageable pageable);


}
