package com.example.springbootloginjwt.payload.Response;

import com.example.springbootloginjwt.models.DepartmentEnum;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UserInfoResponse {
    private Long id;
    private String username;
    private String email;
    private List<String> userRoles;

    private String firstName;

    private String lastName;

    private String phoneNo;

    private double salary;

    private DepartmentEnum department;

    public UserInfoResponse(Long id, String username, String email, List<String> userRoles, String firstName, String lastName, String phoneNo, double salary, DepartmentEnum department) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.userRoles = userRoles;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNo = phoneNo;
        this.salary = salary;
        this.department = department;
    }
}
