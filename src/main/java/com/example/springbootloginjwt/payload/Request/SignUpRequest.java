package com.example.springbootloginjwt.payload.Request;

import com.example.springbootloginjwt.models.DepartmentEnum;
import com.example.springbootloginjwt.models.Role;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Getter
@Setter
public class SignUpRequest {

    @NotBlank
    @Size(min = 3, max = 20)
    private String username;
    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    private Set<Role> role;
    @NotBlank
    @Size(min = 6, max = 40)
    private String password;

    private String firstName;

    private String lastName;

    private String phoneNo;

    private double salary;

    private DepartmentEnum department;
}
