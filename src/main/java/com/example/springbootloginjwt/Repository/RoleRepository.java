package com.example.springbootloginjwt.Repository;

import com.example.springbootloginjwt.models.Role;
import com.example.springbootloginjwt.models.UserRoleEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(UserRoleEnum name);
}
